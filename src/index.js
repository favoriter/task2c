import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  
  const url=req.query.username||'';
  
  //console.log(url);
  const name=canonizeName(url);
  //console.log(name);
  res.send(name);
});
function canonizeName(url){
  const rx= new RegExp('^(@)?(https?:)?(\/\/)?([A-Za-z0-9_.\\-]+\\.[A-Za-z0-9\\-]+)?(\/)?([@A-Za-z0-9_\\.\\-]+)?([\\.\\-=\\?\\w\\d\/]+)?$','i');
  const username= url.match(rx)[6];
  return '@'+username.replace('@','');
}
app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
